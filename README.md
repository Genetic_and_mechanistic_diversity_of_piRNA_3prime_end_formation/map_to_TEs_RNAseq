# measure TE PRKM values (only from sense mapped reads) from RNA seq libraries

This script uses bowtie to map reads to TE consensus sequences. 

## Used tools:

bowtie/0.12.9

samtools/0.1.18

bedtools/2.25.0


## Input:

The input file is a fasta file (tentatively named PE_clipped_trimmed_split.fasta)

The PE_clipped_trimmed_split.fasta looks as follows:

>D00689:115:C86J2ANXX:3:1102:1466:2183#34604#1_AGTCAA/1
TGCTTCTCAGTTCGCAGTGCCCCCTTGCGTTCACGTTA

/1 and /2 mark PE information. /1 reads minus strand and /2 reads plus strand.


## Variables which need to be set: 

**NAMEforANALYSIS**= The name for files and output genrated

**rawFOLDER**= Folder which contains raw files which can be used for further analyses 

**rawOUTPUT**= Folder for output fiels

**counts**= number of reads mapped to the genome

**NSLOTS**= CPUs available for the analysis


## Output

The output file is TE_3MM_rpkm.txt (in case of allowing up to 3 mismatches).

The TE_3MM_rpkm.txt looks as follows:
1360 5.70569
17.6 4.59224

Col1: TE name

Col2: RPKM (only the sense mapping reads are measured.)
