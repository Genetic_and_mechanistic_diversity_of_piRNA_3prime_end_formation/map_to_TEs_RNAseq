### measure TE PRKM values (only from sense mapped reads) from RNA seq libraries

#-------------------------------------------------------------------------------------------------------
#presetting of variables 
NAMEforANALYSIS=

#Folder of files to keep - use for other analysis
rawFOLDER=

#output folder
rawOUTPUT=

#number of reads map to the genome
counts=

#Cores avaiable for computing
NSLOTS=

#bowtie index for TE mapping
TE_index=

#TE names (single line per TE) for adding a pseudocount of 1
TE_names=

# <TE name> <size/bp>
TE_sizes=

#-------------------------------------------------------------------------------------------------------
#prepare all vairables
FOLDER=${rawFOLDER}/${NAMEforANALYSIS}
OUTPUT=${rawOUTPUT}/${NAMEforANALYSIS}/${NAMEforANALYSIS}
#-------------------------------------------------------------------------------------------------------

#create all folders
mkdir -p ${rawOUTPUT}
mkdir -p ${rawFOLDER}/${NAMEforANALYSIS}
mkdir -p ${rawOUTPUT}/${NAMEforANALYSIS}

echo "start"
echo ${NAMEforANALYSIS}
echo ${counts}


######### bowtie allowing only unique mappers (-m 1) and 3MMs from end to end (-v)
### ${FOLDER}_PE_clipped_trimmed_split.fasta looks as follows:
#/1 and /2 mark PE information. /1 reads minus strand and /2 reads plus strand.#>D00689:115:C86J2ANXX:3:1102:1466:2183#34604#1_AGTCAA/1
#TGCTTCTCAGTTCGCAGTGCCCCCTTGCGTTCACGTTA
#>D00689:115:C86J2ANXX:3:1102:1466:2183#34604#2_AGTCAA/1
#CTGCTCCGTCAGGGATCAGAATTCGTTTGGTTGATTTG
#>D00689:115:C86J2ANXX:3:1102:1466:2183#34604#3_AGTCAA/1
#TTGGTTGATTCATCGTCGGTATAGCTGCAGCTCCCTC
cat ${FOLDER}_PE_clipped_trimmed_split.fasta |\
bowtie -f -m 1 -v 3 --sam -p ${NSLOTS} ${TE_index} - |\
samtools view -b -S - | bedtools bamtobed -i - > ${OUTPUT}_TE_3MM.bed


######### /1 and /2 mark PE information. /1 reads minus strand and /2 reads plus strand. The table only includes sense mappers.
awk '($6=="-"&&$4~"/1"||$6=="+"&&$4~"/2"){print $1}' ${OUTPUT}_TE_3MM.bed > ${OUTPUT}_TE_3MM.txt


### add pseudo counts of 1
cat ${TE_names} ${OUTPUT}_TE_3MM.txt | sort | uniq -c |\
######### not include mRNAs:rpl32, act5c, tj, piwi, and ras.
awk -v NAMEforANALYSIS=${NAMEforANALYSIS} -v COUNTS=${counts} -v OFS="\t" 'BEGIN { print "TE_"NAMEforANALYSIS"\t""counts_"NAMEforANALYSIS} ($2!="rpl32"&&$2!="act5c"&&$2!="tj"&&$2!="piwi"&&$2!="ras") { print $2,($1-1)/COUNTS*1000000}' > ${OUTPUT}_TE_3MM_counts.txt


### calculate the RPKM value
paste ${TE_sizes} ${OUTPUT}_TE_3MM_counts.txt | awk -v NAMEforANALYSIS=${NAMEforANALYSIS} 'BEGIN {print "TE "NAMEforANALYSIS} NR>1 {print $1,$4/$2*1000}' > ${OUTPUT}_TE_3MM_rpkm.txt

### ${OUTPUT}_TE_3MM_rpkm.txt looks as follows:
#<TE> <NAMEforANALYSIS>
#1360 5.70569
#17.6 4.59224
#1731 1.5489
#297 3.51621
#3S18 266.291
#412 21.6811
#BS 5.58487
#BS3 0.516206


